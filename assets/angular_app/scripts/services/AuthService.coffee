'use strict'
angular.module('subzapp').factory('AuthService', [
	'$http'
	'API'
	($http, API) ->


		login : (credentials, callbackFn) ->
			$http(
				method : "POST"
				url : "#{API}login"
				data :
					email : credentials.email
					password : credentials.password
			).success callbackFn

])
