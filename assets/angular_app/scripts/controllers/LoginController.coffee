'use strict'
angular.module('subzapp').controller('LoginController', [
	'$scope'
	'$state'
	'$window'
	'AuthService'
	($scope, $state, $window, AuthService) ->

		console.log "login controller"

		$scope.credentials = {
			email : ''
			password : ''
		}

		$scope.login = ->
			AuthService.login $scope.credentials, (response) ->
				console.log response
])
