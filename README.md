nfo

###### I have done a few things to get it working : 
  - Disabled the grunt tasks in 'tasks/register/default.js' i.e. I left the tasks brackets [] empty as this was causing sails to overwrite all the js/ejs and html in the assets folder every time you sails lift(ed)
  - Created an index.ejs in views, this is the root view
  - Changed layout param to 'index' in config/views.js
  - in routes.coffee, added a '/' with a view param that looks for 'index', this will ask sails to render views/index.ejs when the / route is hit, ie. the home page.
  - I have put the angular app inside in the 'angular_app' folder which is inside the assets folder
  - I have integrated bower (http://bower.io) into the app. bower is a package manager for front-end dependencies such as bootstrap, angular, jquery etc.
  - If you want to install something quickly, like a js lib or css lib, just cd into the assets folder and run 'bower install <whatever> --save
  - You can search for bower packages here : http://bower.io/search/
  - the .bowerrc file in the assets folder tells bower to package up any installed libs in the 'assets/bower' folder
  - the bower.json file is what tells bower what to install
  - In views/index.ejs you will see that all the bower stuff is being pulled in, have a look at bower.json to see.
  - run bower update if you change bower.json manually, you can also install stuff by manually putting it into bower.json and then bower update (ing)
  - In the angular section of the assets folder, I have a main app.coffee
  - I have swapped out the normal angular router for a better one (angular-ui-router : https://github.com/angular-ui/ui-router), although I still use the old one just for the 'otherwise' route on app.js config.
  - In the config/routes.js, I have given an example of what I mean by api prefixing, you will also see that I have included an API constant in the app.coffee file, which just has 'api/v1'. Because the app is being served from the same folder as the back-end, you do not need to reference the full url, just the api/v1, as that is how you will separate json requests from the actual request to serve up the initial view, plus it makes debugging sense to see 'api/v1' in the console.
  - I have added a bullshit 'login' method in the WebController.coffee, which just spits back a useless json.
  - The Login Controller is called when the '/' angular state is initialized, which is initialized when the main index.ejs file is loaded, then when the button on logincontroller.coffee is pressed, it calls the 'services/AuthService.coffee' login method, you will be able to follow the trail alright.
